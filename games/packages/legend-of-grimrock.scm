;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages legend-of-grimrock)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (games humble-bundle))

(define-public legend-of-grimrock
  (let* ((arch (match (or (%current-target-system)
                          (%current-system))
                 ("x86_64-linux" "x86_64")
                 ("i686-linux" "x86")
                 (_ ""))))
    (package
      (name "legend-of-grimrock")
      (version "2015-07-07")            ; 1.3.7
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list (string-append "Grimrock-Linux-" version ".sh")))))
         (file-name (string-append "Grimrock-Linux-" version ".sh"))
         (sha256
          (base32
           "0j9ciyf8wn2k3rf6lyxv61ickmbyppngjric7ipdvgk8g948fs47"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,(string-append "target/Grimrock.bin." arch)
            ("gcc:lib" "freeimage" "freetype" "minizip" "zlib" "openal" "mesa" "sdl2"
             "libx11" "libvorbis")))
         #:install-plan
         `(("target" (".") "share/grimrock/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "bash")
                       (assoc-ref inputs "source")
                       "--tar" "xf")
               #t))
           (add-after 'unpack 'extract-archives
             (lambda _
               (mkdir-p "target")
               (and (system (format #f "xz -d < instarchive_all | tar xf - -C target"))
                    (system (format #f "xz -d < instarchive_linux_~a | tar xf - -C target"
                                    ,arch))
                    #t)))
           (add-after 'install 'make-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/grimrock"))
                      (real (string-append output "/share/grimrock/Grimrock.bin." ,arch))
                      (icon (string-append output "/share/grimrock/grimrock.png")))
                 (mkdir-p (dirname wrapper))
                 (symlink real wrapper)
                 (make-desktop-entry-file (string-append output "/share/applications/grimrock.desktop")
                                          #:name "Legend of Grimrock"
                                          #:exec wrapper
                                          #:icon icon
                                          #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("freeimage" ,freeimage)
         ("freetype" ,freetype)
         ("minizip" ,minizip)
         ("zlib" ,zlib)
         ("openal" ,openal)
         ("mesa" ,mesa)
         ("sdl2" ,sdl2)
         ("libx11" ,libx11)
         ("libvorbis" ,libvorbis)))
      (home-page "http://www.grimrock.net/")
      (synopsis "3D grid-based, real-time dungeon crawler")
      (description
       "Legend of Grimrock is a dungeon crawling role playing game with an
oldschool heart but a modern execution.  A group of prisoners are sentenced to
certain death by exiling them to the secluded Mount Grimrock for vile crimes
they may or may not have committed.  Unbeknownst to their captors, the mountain
is riddled with ancient tunnels, dungeons and tombs built by crumbled
civilizations long perished now.  If they ever wish to see daylight again and
reclaim their freedom the ragtag group of prisoners must form a team and descend
through the mountain, level by level.

The game brings back the oldschool challenge with highly tactical real-time
combat and grid-based movement, devious hidden switches and secrets as well as
deadly traps and horrible monsters.  Legend of Grimrock puts an emphasis on
puzzles and exploration and the wits and perception of the player are more
important tools than even the sharpest of swords could be.  And if you are a
hardened dungeon crawling veteran and you crave an extra challenge, you can arm
yourself with a stack of grid paper and turn on the Oldschool Mode which
disables the luxury of the automap! Are you ready to venture forth and unravel
the mysteries of Mount Grimrock?

@itemize
@item Explore a vast network of ancient tunnels, discover secrets and find a way
to survive in the perilous dungeons of Mount Grimrock.
@item Cast spells with runes, craft potions with herbs and fight murderous
monsters with a wide variety of weaponry.
@item Create a party of four characters and customize them with different races,
classes, skills and traits.
@item Pure blooded dungeon crawling game with grid-based movement and thousands
of squares riddled with hidden switches, pressure plates, sliding walls,
floating crystals, forgotten altars, trapdoors and more.
@end itemize\n")
      (license (undistributable "No URL")))))
