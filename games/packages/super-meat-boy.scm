;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages super-meat-boy)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages sdl)
  #:use-module (nongnu packages game-development)
  #:use-module (games humble-bundle))

(define-public super-meat-boy
  (let* ((arch (match (or (%current-target-system)
                          (%current-system))
                 ("x86_64-linux" "amd64")
                 ("i686-linux" "x86")
                 (_ ""))))
    (package
      (name "super-meat-boy")
      (version "11112013")
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list (string-append "supermeatboy-linux-"
                                           version "-bin")))))
         (file-name (string-append "supermeatboy-linux-"
                                           version "-bin"))
         (sha256
          (base32
           "130p0xwgq728x25vqir3xssc7qzk0mcgv11q0nvimld1q4z5q9kc"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,(string-append "data/" arch "/SuperMeatBoy")
            ("gcc:lib" "openal" "sdl2"
             "out"
             ,,@(if (string-prefix? "x86_64" (or (%current-target-system)
                                                 (%current-system)))
                    '()
                    '("libsteam"))))
           ,,@(if (string-prefix? "x86_64" (or (%current-target-system)
                                               (%current-system)))
                  `('(,(string-append "data/" arch "/libsteam_api.so")
                      ("gcc:lib")))
                  '()))
         #:validate-runpath? #f    ; TODO: libsteam_api.so does not pass, why?
         #:install-plan
         `(("data/Levels" (".") "share/super-meat-boy/Levels/")
           ("data/resources" (".") "share/super-meat-boy/resources/")
           ("data/" ("README-licenses.txt" "README-linux.txt"
                     "buttonmap.cfg" "gameaudio.dat" "gamedata.dat"
                     "locdb.txt" "supermeatboy.png")
            "share/super-meat-boy/")
           (,,(string-append "data/" arch) ("SuperMeatBoy")
            ,,(string-append "share/super-meat-boy/" arch "/"))
           (,,(string-append "data/" arch)
            (,,@(if (string-prefix? "x86_64" (or (%current-target-system)
                                                 (%current-system)))
                    '("libsteam_api.so")
                    '()))
            "lib/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               ;; We use system* to ignore errors.
               (system* (which "unzip")
                        (assoc-ref inputs "source"))
               #t))
           (add-after 'install 'fix-mariadb-path
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (mariadb (assoc-ref inputs "mariadb-connector-c")))
                 (symlink (string-append mariadb "/lib/mariadb/libmariadb.so")
                          (string-append output "/lib/libmariadb.so.1")))
               #t))
           (add-after 'install 'make-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/supermeatboy"))
                      (real (string-append output "/share/super-meat-boy/" ,arch "/SuperMeatBoy"))
                      (icon (string-append output "/share/super-meat-boy/supermeatboy.png")))
                 (mkdir-p (dirname wrapper))
                 (symlink real wrapper)
                 (make-desktop-entry-file (string-append output "/share/applications/supermeatboy.desktop")
                                          #:name "Super Meat Boy"
                                          #:exec wrapper
                                          #:icon icon
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("openal" ,openal)
         ("sdl2" ,sdl2)
         ("mariadb-connector-c" ,mariadb-connector-c)
         ,@(if (string-prefix? "x86_64" (or (%current-target-system)
                                            (%current-system)))
               '()
               `(("libsteam" ,libsteam)))))
      (home-page "http://www.supermeatboy.com/")
      (synopsis "Insanely hard and delightfully meaty platformer")
      (description
       "The player controls Meat Boy, a red, cube-shaped character, as he
attempts to rescue his girlfriend, Bandage Girl, from the game's antagonist
Dr.@tie{}Fetus.  The gameplay is characterized by fine control and split-second
timing, as the player runs and jumps through over 300 hazardous levels while
avoiding obstacles.  The game also supports the creation of player-created
levels.")
      (license (undistributable "No URL")))))
