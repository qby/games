;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2020 Efraim Flashner <efraim@flashner.co.il>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages limbo)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (nongnu packages game-development)
  #:use-module (games humble-bundle))

(define-public limbo
  (package
    (name "limbo")
    (version "2014-06-18")
    (source
     (origin
       (method humble-bundle-fetch)
       (uri (humble-bundle-reference
             (help (humble-bundle-help-message name))
             (config-path (list (string->symbol name) 'key))
             (files (list (string-append "Limbo-Linux-"
                                         version
                                         ".sh")))))
       (file-name (string-append "Limbo-Linux-" version ".sh"))
       (sha256
        (base32
         "1lr73zqi07d9cj4vj58rgwj9p39377ff32nr9n0jacixmy53r177"))))
    (build-system binary-build-system)
    (supported-systems '("i686-linux" "x86_64-linux"))
    (arguments
     `(#:system "i686-linux"
       #:patchelf-plan
       `(("data/limbo"
          ("gcc:lib" "mesa" "sdl2")))
       #:install-plan
       `(("data/data" (".") "share/limbo/data/")
         ("data" ("limbo" "README-linux.txt" "settings.txt") "share/limbo/"))
       #:phases
       (modify-phases %standard-phases
         (replace 'unpack
           (lambda* (#:key inputs #:allow-other-keys)
             (invoke (which "makeself_safeextract")
                     "--mojo"
                     (assoc-ref inputs "source"))
             (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
             #t))
         (add-after 'install 'make-wrapper
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((output (assoc-ref outputs "out"))
                    (wrapper (string-append output "/bin/limbo"))
                    (real (string-append output "/share/limbo/limbo"))
                    (icon (string-append output "/share/limbo/limbo.png")))
               (mkdir-p (dirname wrapper))
               (symlink real wrapper)
               (make-desktop-entry-file (string-append output "/share/applications/limbo.desktop")
                                        #:name "Limbo"
                                        #:exec wrapper
                                        #:icon icon
                                        #:categories '("Application" "Game")))
             #t)))))
    (native-inputs
     `(("makeself-safeextract" ,makeself-safeextract)))
    (inputs
     `(("gcc:lib" ,(canonical-package gcc) "lib")
       ("mesa" ,mesa)
       ("sdl2" ,sdl2)))
    (home-page "https://playdead.com/games/limbo/")
    (synopsis "2D side-scroller, incorporating a physics system")
    (description
     "Limbo is a 2D side-scroller, incorporating a physics system that governs
environmental objects and the player character.  The player guides an unnamed
boy through dangerous environments and traps as he searches for his sister.  The
developer built the game's puzzles expecting the player to fail before finding
the correct solution.  Playdead called the style of play \"trial and death\",
and used gruesome imagery for the boy's deaths to steer the player from
unworkable solutions.

The game is presented in black-and-white tones, using lighting, film grain
effects and minimal ambient sounds to create an eerie atmosphere often
associated with the horror genre.")
    (license (undistributable "No URL"))))
