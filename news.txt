;; -*- mode: scheme; -*-

(channel-news
 (version 0)
 (entry (commit "25387230a7f99c2b0886488ed061747c9e00434a")
        (title (en "New configuration format")
               (fr "Nouveau format de configuration"))
        (body (en "The configuration file games.scm is now evaluated instead
of just being read.  Besides, it must evalute to a <gaming-config> object.
See the readme.org file for an example and more details.")
              (fr "Le fichier de configuration games.scm est dorénavant évalué
plutôt que d'être seulement lu.  De plus, le fichier doit retourner un objet
<gaming-config>.  Le fichier readme.org donne un exemple et plus de
détails."))))
